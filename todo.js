var statuses = {todo:'A faire',paused:'En cours', started:'Commencé', cancelled:'Annulé',standby:'Standby', done:'Fait'};
var months = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre'];
var columns = {
	creation:0,
	id:1,
	project:2,
	topic:3,
	description:4,
	priority:5,
	schedule:6,
	status:7,
	history:8,
	comments:9
}
listeners = _new(Listeners);

function get_current_day(){
	return (new Date).get_full_date();
}

/*
 * Objects definitions
 */

var ProjectList = _extend([],{
	type:'ProjectList',
	selected:null,
	init:function(){
		// Parse current the HTML project list if there is one
		var pl = document.getElementById('project_list');

		for(const p of pl.children){
			var project = _new(Project, p.childNodes[1].data, p);
			this.push(project);

			// Update pointer to selected project
			var input = project.firstElementChild;
			if(input.checked){
				ProjectList.selected = project;
			}
		}
	},
	add:function(projectName){
		var project = _new(Project, projectName);
		this.push(project);
		ProjectList.selected = project;
		document.getElementById('project_list').appendChild(project.container);
		return project;
	},
	search:function(projectName){
		/* Return corresponding project if found, null otherwise */
		ret = null;
		for(var i=0;i<this.length;i++){
			if(this[i].name === projectName){
				ret = this[i];
				break;
			}
		}
		return ret;
	},
	remove:function(projectName){
		for(var i = 0;i < this.length;i++){
			if(this[0].name === projectName){
				this.shift();
			}
			else{
				this.push(this.shift());
			}
		}
	},
	toString:function(){
		return this.map(function(x){return x.name},this).join('\n');
	}
});


var Project = _extend(_Element,{
	type:'Project',
	init:function(name){
		var args = slice(arguments,1);

		if(args.length === 0){
			var input = document.createElement('input');
			input.type = 'radio';
			input.name = 'project_mgmt';
			input.checked = 'true';
			_super(_Element, this,['p',{'class':'project_list_item'},input,document.createTextNode(name)]);
		}
		else{
			_super(_Element, this, args);
		}
		this.name = name;

		//listen for selection state change
		this.listen_selection();
	},
	listen_selection:function(){
		var input = this.firstElementChild;
		listeners.add(input,'change',this);	
	},
	handleEvent:function(e){
		if(e.type === 'change'){
			if(e.target.checked){
				ProjectList.selected = e.target.parentNode.jsobj;
			}
		}
	},
	rename:function(newName){
		this.name = newName;
	},
	toString:function(){return this.name;}
});

projectList = _new(ProjectList);

var TaskList  = _extend([],{
	type:'TaskList',
	latestId:0,
	init:function(){
		var tbody = document.querySelector('#task_list');
		if(tbody === null){
			this.root = _new(_Element,'tbody',{'id':'task_list'});
		}
		else{
			//Parse HTML task list if it exists
			this.root = _new(_Element, tbody);
			for(const row of tbody.children){
				var task = _new(Task,row);
				[].push.call(this,task);
				TaskList.latestId = Math.max(TaskList.latestId,task.id);
			}
		}

	},
	add:function(){
		var task = _new(Task);
		this.push(task);
		TaskList.latestId += 1;
	},
	push:function(task){
		[].push.call(this,task);
		this.root.append(task.row);
	},
	update_project_lists:function(projectName){
		/* Add projectName to the projects dropdown if it's not already there */

		for(var i = 0 ; i < this.length ; i++){
			var row = this[i].row;
			var projectCell = row.get_child(columns.project);
			var dropdown = projectCell.get_child(0);
			var option = dropdown.get_child(0);
			var upToDate = false;

			//Iterate on the options in the list until a match is found
			for (var j = 0;option !== null;j++){
				if(option.value === projectName){
					upToDate = true;
					break;
				}
				option = dropdown.get_child(j);
			}

			//If no match was found, add an option to the dropdown
			if(!upToDate){
				var newOption =_new(_Element,'option',{'class':'project_dropdown_option'},projectName);
				newOption.value = projectName;
				dropdown.append(newOption);
			}
		}
	},
	remove:function(task){
		for(var i = 0;i < this.length;i++){
			if(this[0].id === task.id){
				t = this.shift();
				delete t;
			}
			else{
				this.push(this.shift());
			}
		}
	}
});


var Task = {
	type:'Task',

	init:function(row=null){
		var _this = this; // for listeners

		// Set fields to defaults
		this.creation = new Date();
		this.id = TaskList.latestId + 1;
		this.project = ProjectList.selected;
		this.topic = '';
		this.description = '';
		this.priority = 2;
		this.schedule = new Date();
		this.status = statuses.todo;
		this.history = '';
		this.comments = '';

		this.startTime = 0;
		this.reports = {};

		// Create sell objects
		if(row === null){
			this.row = _new(TableRow);
			var creationCell = _new(ScrollableCell,value=this.creation.get_full_date(),placeholder='Creation date','td',{'class':'task_creation_cell'}).disable();
			var idCell = _new(ScrollableCell,value=this.id,placeholder='','td',{'class':'task_id_cell'}).disable();
			var projectCell = _new(DropdownCell,projectList.map(function(x){return x.name},projectList));
			var topicCell = _new(ScrollableCell,value=this.topic,placeholder='Thème');
			var descriptionCell = _new(ScrollableCell,value=this.description,placeholder='Description');
			var priorityCell = _new(DropdownCell,[1,2,3,4]);
			var scheduleCell = _new(DatePickerCell);
			var statusCell = _new(DropdownCell,statuses.values());
			var historyCell =_new(ScrollableCell).disable(); 
			var commentsCell = _new(ScrollableCell,value='',placeholder='Commentaire');

			priorityCell.get_child(0).add_class('priority_' + this.priority);
			statusCell.get_child(0).add_class('status_todo');

			this.row.append(creationCell, idCell, projectCell, topicCell, descriptionCell, priorityCell, scheduleCell, statusCell, historyCell, commentsCell);
		}
		else{
			// If a row is given, use it for initialization, else use defaults
			this.row = _new(TableRow, container=row);

			// creation

			var creation = row.firstElementChild.firstElementChild.value;
			creation = creation.split('/');
			this.creation = new Date(creation[2], creation[1] - 1, creation[0]);
			var creationCell = _new(ScrollableCell,value=this.creation.get_full_date(),placeholder='Creation date',container = get_element_child(row,0),{'class':'task_creation_cell'}).disable();
			this.row.children.push(creationCell);

			// id
			this.id = parseInt(get_element_child(row,1).firstElementChild.value);
			var idCell = _new(ScrollableCell,value=this.id,placeholder='',container=get_element_child(row,1), {'class':'task_id_cell'}).disable();
			this.row.children.push(idCell);

			// project
			var projectDropdown = get_element_child(row,2).firstElementChild;
			var projectName = projectDropdown.selectedOptions[0].value;
			var project = projectList.search(projectName);
			if(project === null){
				project = projectList.add(projectName);
			}
			this.project = project;
			var projectCell = _new(DropdownCell,projectList.map(function(x){return x.name},projectList),get_element_child(row,2));
			this.row.children.push(projectCell);

			// topic
			this.topic = get_element_child(row,3).firstElementChild.value;
			var topicCell = _new(ScrollableCell,value=this.topic,placeholder='Thème',get_element_child(row,3));
			this.row.children.push(topicCell);

			// description
			this.description = get_element_child(row,4).firstElementChild.value;
			var descriptionCell = _new(ScrollableCell,value=this.description,placeholder='Description',get_element_child(row,4));
			this.row.children.push(descriptionCell);

			// priority
			var priorityDropdown = get_element_child(row,5).firstElementChild;
			this.priority = parseInt(priorityDropdown.selectedOptions[0].value);
			var priorityCell = _new(DropdownCell,[1,2,3,4], get_element_child(row,5));
			this.row.children.push(priorityCell);

			// schedule
			this.schedule = get_element_child(row,6).firstElementChild.valueAsDate;
			var scheduleCell = _new(DatePickerCell, get_element_child(row,6));
			scheduleCell.value = this.schedule.get_full_date();
			this.row.children.push(scheduleCell);

			// status
			var statusDropdown = get_element_child(row,7).firstElementChild;
			this.status = statusDropdown.selectedOptions[0].value;
			var statusCell = _new(DropdownCell,statuses.values(), get_element_child(row,7));
			this.row.children.push(statusCell);

			// history
			this.history = get_element_child(row,8).firstElementChild.value;
			var historyCell =_new(ScrollableCell,value=this.history,placeholder='', container=get_element_child(row,8)).disable(); 
			this.row.children.push(historyCell);

			// comments
			this.comments = get_element_child(row,9).firstElementChild.value;
			var commentsCell = _new(ScrollableCell,value='',placeholder='Commentaire', container=get_element_child(row,9));
			this.row.children.push(commentsCell);
		}

		// Select appropriate values for the dropdowns
		projectCell.select(this.project.toString());
		priorityCell.select(this.priority.toString());
		statusCell.select(this.status.toString());

		// Set appropriate classes to color dropdown options
		priorityCell.color_options(_new(KVList).from_list([
			['1','priority_1'],
			['2','priority_2'],
			['3','priority_3'],
			['4','priority_4'],
		]));

		statusCell.color_options(_new(KVList).from_list([
			[statuses.todo,'status_todo'],
			[statuses.done,'status_done'],
			[statuses.paused,'status_paused'],
			[statuses.started,'status_started'],
			[statuses.cancelled,'status_cancelled'],
			[statuses.standby,'status_standby'],
		]));
		
		// Set listeners to update the task on UI change
		(function(){
			var task = _this;

			//Project cell update listener
			listeners.add(projectCell,'change',function(e){
				task.project = projectList.search(projectCell.value);
			});
			
			//Topic cell update listener
			listeners.add(topicCell,'change',function(e){
				topicCell.value = e.target.value;
				task.topic = topicCell.value;
			});

			//Description cell update listener
			listeners.add(descriptionCell,'change',function(e){
				descriptionCell.value = e.target.value;
				task.description = descriptionCell.value;
			});

			//Priority cell update listener
			listeners.add(priorityCell,'change',function(e){
				task.priority = priorityCell.value;

				var dropdown = priorityCell.get_child(0);
				var optionClassList = priorityCell.get_selected().class.split(' ');
				var dropdownClassList = dropdown.class.split(' ');

				for (const className of dropdownClassList){
					if(className.startsWith('priority_')){
						dropdown.remove_class(className);
					}
				}

				for(const className of optionClassList){
					if(className.startsWith('priority_')){
						dropdown.add_class(className);
						break;
					}
				}
			});

			//Schedule cell update listener
			listeners.add(scheduleCell.firstElementChild,'change',function(e){
				task.schedule = scheduleCell.firstElementChild.valueAsDate;
				scheduleCell.value = scheduleCell.firstElementChild.valueAsDate.get_full_date();
			});

			//Status cell update listener
			listeners.add(statusCell.firstElementChild,'change',function(e){
				statusCell.value = e.target.value;
				var dropdown = statusCell.get_child(0);
				var optionClassList = statusCell.get_selected().class.split(' ');
				var dropdownClassList = dropdown.class.split(' ');

				for (const className of dropdownClassList){
					if(className.startsWith('status_')){
						dropdown.remove_class(className);
					}
				}

				for(const className of optionClassList){
					if(className.startsWith('status_')){
						dropdown.add_class(className);
						break;
					}
				}
				if(statusCell.value !== statuses.started){
					if(task.status === statuses.started){
						task.update_report(task.reports[get_current_day()]);
						report.generate();
					}
				}
				else{
					task.start();
				}
				task.status = statusCell.value;
				task.update_history();
			});

			//Comments cell update listener
			listeners.add(commentsCell,'change',function(e){
				commentsCell.value = e.target.value;
				task.comments = commentsCell.value;
			});
		})();
	},

	reset_reports:function(){
		/* Reset all duration in task reports to 0 */
		for(const day of this.reports.keys()){
			this.reports[day].duration = 0;
		}
	},
	remove:function(){
		taskList.remove(this);
	},
	start:function(){
		/* Initialize or update task reports when task is started */
		this.startTime = new Date();
		if(get_current_day() in this.reports){
			this.update_report(this.reports[get_current_day()]);
		}
		else{
			this.reports[get_current_day()] = _new(TaskReport,this.project,this.topic,0);
		}
	},
	update_report:function(report){
		report.update(this.get_duration(),this.project,this.topic);
	},
	get_duration:function(){
		return new Date() - this.startTime;
	},
	update_history:function(){
		/* Update history on task status change */
		var currentDate = new Date();
		var currentDay = currentDate.get_full_date();
		var currentTime = currentDate.getHours() + ':' + currentDate.getMinutes();
		var historyCell = this.get_column(columns.history);

		if(this.history === ''){
			this.history = currentDay + ' @ ' +  currentTime + '->' + this.status;
		}
		else{
			this.history = currentDay + ' @ ' +  currentTime + '->' + this.status + '\n' + this.history;
		}

		historyCell.set_value(this.history);
	},
	get_column:function(i){
		return this.row.get_child(i);
	},
	hide:function(b=true){
		var visibility = b ? 'collapse':'visible';
		this.row.set_style('visibility',visibility);
	}
}

var TaskReport = {
	type:'TaskReport',
	init:function(project,topic,duration){
		this.project = project;
		this.topic = topic;
		this.duration = duration; //In milliseconds
	},
	update:function(duration,project=null,topic=null){
		if(project !== null){
			this.project = project;
		}
		if(topic !== null){
			this.topic = topic
		}
		this.duration += duration;		
	}
}

var taskList = _new(TaskList);

var Report = {
	type:'Report',
	init:function(){
		this.content = {};
		this.week = _new(Week);

		var weeklyId = this.week[0].get_full_date();
		var weeklyReportSection = document.getElementById(weeklyId);
		var reportWeekHeader = _new(_Element,'div',{'class':'report_week_cell'}, 'Semaine du ' + this.week[0].get_full_date());
		var weekSectionExists = this.check_week_section_exists();

		if(weekSectionExists){
			//If HTML section already exists for this week, attach it to the report 
			this.weekContainer = _new(_Element, weeklyReportSection);
			this.table = _new(_Element, weeklyReportSection.firstElementChild);
			var header = _new(_Element, this.table.firstElementChild);

			var reportWeekHeader = _new(_Element, get_element_child(header.container,0));
			header.children.push(reportWeekHeader);
			for(var i = 1; i <= 5; i++){
				var weekDayHeader = _new(_Element, get_element_child(header.container,i));
				header.children.push(weekDayHeader);
			}

			this.table.children.push(header);

			// Parse the values already in the report
			for(var i=1;;i++){
				var row = get_element_child(this.table.container,i);
				if(row === null || row === undefined){
					break;
				}

				var reportRow = _new(_Element, row);
				var projectCell = _new(_Element, row.firstElementChild);
				projectCell.value = row.firstElementChild.innerHTML;
				reportRow.children.push(projectCell);

				for(var j = 1; j <= 5; j++){
					var reportCell = get_element_child(row,j);
					var summary = reportCell.firstElementChild;

					//Parse duration
					var durationString = summary.innerHTML;
					var r = new RegExp('(\\d+)h(\\d+)m(\\d+)s');
					var match = durationString.match(r);
					var groups = [].map.call(match.slice(1),x=>parseInt(x));
					var duration = groups[0]*3600 + groups[1]*60 + groups[2];

					//Parse topics list
					var topicsList = get_element_child(reportCell, 1);
					var topicsKVList = _new(KVList);
					var items = [];
					for(var k = 0; k < topicsList.children.length; k++){
						//Parse topic duration
						var listItem = get_element_child(topicsList, k);
						li = _new(_Element, 'li',{'class':'topic_list_item'});
						items.push(li);
						li.container = listItem;
						var topicDurationString = listItem.innerHTML;
						var r = new RegExp('(.*):\s*(\\d+)h(\\d+)m(\\d+)s');
						var match = topicDurationString.match(r);
						var topic = match[1];
						var groups = [].map.call(match.slice(2),x=>parseInt(x));
						var topicDuration = groups[0]*3600 + groups[1]*60 + groups[2];
						topicsKVList.set(topic, topicDuration);
					}
					var cell = _new(ReportCell,duration,topicsKVList);
					cell.container = reportCell;
					cell.summary.container = summary;
					cell.topicList.container = topicsList;
					cell.topicList.children = items;
					cell.children = [cell.summary,cell.topicList];
					reportRow.children.push(cell);
				}
				this.table.children.push(reportRow);
			}

		}
		else{
			//If HTML section does not already exist for this week, create it
			this.create_week_section();
		}
	},
	generate:function(){
		/*  Generate or update report */
		this.week.set_current();
		if(!this.check_week_section_exists()){
			this.create_week_section();
		}
		this.content = {}
		this.gather_data();

		for (const project in this.content){
			if(typeof this.content[project] === 'function') continue;
			var projectReports = this.content[project];
			var reportRow = this.table.get_child(1);
			var inReport = false;

			// Check whether the project is already in the report
			for(var row=1; reportRow !== null && !inReport; row++){
				var projectHeader = reportRow.get_child(0);
				if(projectHeader.value === project){
					inReport = true;
					break;
				}
				reportRow = this.table.get_child(row+1);
			}

			// If the project is not in the report, add it 
			if(!inReport){
				reportRow = _new(_Element,'div',{'class':'report_row'});
				reportRow.append(_new(_Element,'div',{'class':'report_row_header'},project));

				for(var i=0;i<5;i++){
					var day = this.week[i].get_full_date();

					if(day in projectReports){
						var dayReport = projectReports[day];
						var duration = dayReport['total'];
						var topics = dayReport['topics']
						reportRow.append(_new(ReportCell, duration, topics));
					}
					else{
						reportRow.append(_new(ReportCell));
					}
				}
				this.table.append(reportRow);
			}
			// If the project is already in the report, update the values
			else{
				reportRow = this.table.get_child(row);

				for(var i=0;i<5;i++){
					var day = this.week[i].get_full_date();

					if(day in projectReports){
						var dayReport = projectReports[day];
						var duration = dayReport['total'];
						var topics = dayReport['topics']
						var dayReportCell = reportRow.get_child(i+1);
						dayReportCell.update(duration,topics);
					}
				}
			}
		}
		// Reset durations in task reports
		for(var i = 0; i < taskList.length; i++){
			taskList[i].reset_reports();
		}
	},
	check_week_section_exists:function(){
		this.week.set_current();
		var weeklyId = this.week[0].get_full_date();
		var weeklyReportSection = document.getElementById(weeklyId);

		return weeklyReportSection !== null;
	},
	create_week_section:function(){
		var weeklyId = this.week[0].get_full_date();
		this.table = _new(_Element,'div',{'class':'weekly_report_table'});
		this.weekContainer = _new(_Element,'section',{'id':weeklyId,'class':'weekly_report_section'})

		var header = _new(_Element,'div',{'class':'report_header'});
		var reportWeekHeader = _new(_Element,'div',{'class':'report_week_cell'}, 'Semaine du ' + weeklyId);
		var reportSection = document.querySelector('section.report');

		if(document.querySelectorAll('section.report>section.weekly_report_section').length === 0){
			reportSection.appendChild(this.weekContainer.container);
		}
		else{
			reportSection.insertBefore(this.weekContainer.container,get_element_child(reportSection,1));
		}
		this.weekContainer.append(this.table);
		this.table.append(header);
		header.append(reportWeekHeader);
		var weekDayIterator = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi'].values();
		for(var day = weekDayIterator.next(); !day.done; day = weekDayIterator.next()){
			var weekDayHeader = _new(_Element,'div',{'class':'report_header_cell'},day.value);
			header.append(weekDayHeader);
		}
	},
	gather_data:function(){
		/* Consolidate current durations of all tasks */
		for(const task of taskList){
			var days = task.reports.keys();

			for(const day of days){
				// Ignore task reports that are not for the correct day
				if(!this.week.is_in_week(day)){
					continue;
				}

				var taskReport = task.reports[day];
				var project = taskReport.project.name;
				var topic = taskReport.topic;
				var duration = Math.ceil(taskReport.duration/1000); //in seconds

				// If the report already contains the project, update report
				// accordingly. Otherwise create the entry
				if(project in this.content){
					// If task's day is present in the report for this project,
					// update total duration and duration for the topic.
					// Otherwise create the entry
					if(day in this.content[project]){
						this.content[project][day]['total'] += duration;

						if(this.content[project][day]['topics'].contains(topic)){
							duration += this.content[project][day]['topics'].get(topic);
						}

						this.content[project][day]['topics'].set(topic,duration);
					}
					else{
						this.content[project][day] = {
							'topics':_new(KVList, topic, duration),
							'total':duration
						};
					}
				}
				else{
					this.content[project] = {};
					this.content[project][day] = {
						'topics':_new(KVList, topic, duration),
						'total':duration
					};
				}
			}
		}
	}
};
var report = _new(Report);

var Criterion = {
	type:'Criterion',
	init:function(column,values,operator='='){
		this.column = column;
		this.operator = operator;
		this.values = values;
	},
	match:function(task){
		/* Check if task matches the criterion */
		var ret = true;
		var columnIndex = typeof this.column === "number" ? this.column : columns[this.column];
		var columnValue = task.get_column(columnIndex).value;

		switch(this.operator){
			case 'equal':
			case '=':
				// Check if the cell value equals one of the values in the criterion
				if(!this.values.includes(columnValue)){
					ret = false;
				}
				break;
			case 'containsany':
				// Check if the cell value contains one of the values in the criterion
				ret = false;
				for(const testValue of this.values){
					if(testValue === ''){
						if(columnValue === testValue){
							ret = true;
							break;
						}
						else{
							break;
						}
					}
					if(columnValue.includes(testValue)){
						ret = true;
						break;
					}
				}
				break;
		}
		return ret;
	}
}

var Filter = {
	type:'Filter',
	init:function(){
		this.criteria = [];
		for(const criterion of arguments){
			this.criteria.push(criterion);
		}
	},
	add:function(criterion){
		/* Add or update a criterion to the filter */

		var update = false;
		for(var i = 0; i < this.criteria.length; i++){
			// If there already is a criterion for the column, update it
			if(this.criteria[i].column === criterion.column){
				this.criteria[i] = criterion;
				update = true;
				break;
			}
		}
		// If no already existing criterion for this column was found, add it
		if(!update){
			this.criteria.push(criterion);
		}
	},
	remove:function(){
		/* Remove critera from filter */

		// If no argument was provided, remove all criteria
		if(arguments.length === 0){
			for(var i = 0; i < this.criteria.length; i++){
				delete this.criteria.pop();
			}
		}
		else{
			// TODO: make sure this works as intended
			for(const criterion of arguments){
				var newCriteria = [];

				for(var i = 0 ; i < this.criteria.length; i++){
					if(criterion !== this.criteria[i].column){
						newCriteria.push(this.criteria[i]);
					}
				}

				this.criteria = newCriteria;
			}
		}
	},
	apply:function(){
		/* Hide tasks that don't match the filter */

		for(var i = 0; i < taskList.length; i++){
			var task = taskList[i];
			task.hide(false);
			for(const criterion of this.criteria){
				if(!criterion.match(task)){
					task.hide();
					break;
				}
			}
		}
	},
	contains:function(column, value){
		var result = false;
		var columnIndex = typeof column === "number" ? column : columns[column];

		for(const criterion of this.criteria){
			if (criterion.column != columnIndex){
				continue;
			}
			if (criterion.values.includes(value)){
				result = true;
				break;
			}
		}
		return result;
	}
}
filter = _new(Filter);

/*
 * Listeners
 */

var headers = document.querySelectorAll('#task_table .header_area');
var filterMenu = null;

// Display filter menu on click on headers
for(var i = 0; i < headers.length; i++){
	var header = headers[i];
	var classList = header.className.split(' ');

	switch(i){
		case 0://Creation
		case 1://ID
		case 9://Comment
			continue;
			break;
		case 2://Project
		case 3://Topic
		case 4://Description
		case 5://Priority
		case 7://Status
			(function(){
				var j = i;
				var headerj = header;
				listeners.add(headerj, 'click', function(e){
					//Get all the values in the column
					var values = [];
					for(const task of taskList){
						var value = task.row.get_child(j).value;
						if(!values.includes(value)){
							values.push(value);
						}
					}
					values.sort();

					//Display the menu
					filterMenu = _new(FilterMenu, headerj, values);
					e.stopPropagation();
					display_menu(header, filterMenu.container,e);
				});}
			)();
			break;
		case 6://Schedule
			(function(){
				var j = i;
				var headerj = header;
				listeners.add(headerj, 'click', function(e){
					var dates = [];
					var filterMenu = _new(FilterMenu, headerj, []);

					// Get all dates in the column
					for(const task of taskList){
						var value = task.row.get_child(j)._container_.firstElementChild.valueAsDate;
						//Ignore duplicates
						if(!dates.map(d => d.getTime()).includes(value.getTime())){
							dates.push(value);
						}
					}

					// Create the hierarchy of dates => Year -> Month -> Date (dd/mm/yyyy)
					if(dates.length > 0){
						dates.sort(function(d1,d2){return d1==d2?0:d1<d2?-1:1;}); // Not sure the sort actually needs an argument
						var dateTree = {};
						var currentYearFolder = _new(Folder,text=dates[0].getFullYear(),folded=false,values=[],margin=0);
						var currentMonthFolder = currentYearFolder.add_subfolder(months[dates[0].getMonth()], false);

						filterMenu.add_folder(currentYearFolder);

						var currentYear = dates[0].getFullYear();
						var currentMonth = dates[0].getMonth();
						var currentDay = dates[0].getDate();

						// Create a new Folder for each new year, and for each new month
						for(const value of dates){
							if(value.getFullYear() !== currentYear){
								currentYearFolder = _new(Folder,text=value.getFullYear(),folded=false,values=[],margin=0);
								currentMonthFolder = currentYearFolder.add_subfolder(months[value.getMonth()],false);
								filterMenu.add_folder(currentYearFolder);
								currentYear = value.getFullYear();
								currentMonth = value.getMonth();
							}
							else{
								if(value.getMonth() !== currentMonth){
									currentMonthFolder = currentYearFolder.add_subfolder(months[value.getMonth()],false);
									currentMonth = value.getMonth();
								}
								var item = currentMonthFolder.add_values([value.get_full_date()])[0];
								if (!filter.contains(columns.schedule, item.value)){
									item.selected = false;
								}
								filterMenu.items.push(item);
							}
						}
					}

					// Display menu
					e.stopPropagation();
					display_menu(header, filterMenu.container,e);
				});}
			)();
			break
		case 8://History
			(function(){
				var j = i;
				var headerj = header;
				listeners.add(headerj, 'click', function(e){
					// Get all the dates in all the histories
					var values = [];
					for(const task of taskList){
						var history = task.row.get_child(j).value;
						var historyLines = history.split('\n');
						for(const line of historyLines){
							if(line === ''){
								values.push(line);
								continue;
							}
							var date = line.slice(0,10);
							if(!values.includes(date)){
								values.push(date);
							}
						}
					}

					// Display menu
					filterMenu = _new(FilterMenu, headerj, values, 'containsany');
					e.stopPropagation();
					display_menu(header, filterMenu.container,e);
				});
			})();
			break;
	}
}

// Remove context menu when clicking anywhere on the page
var body = document.getElementsByTagName('body')[0];
body.addEventListener('click', function(e){
	if(contextMenu !== null){ 
		if(!contextMenu.contains(e.target)){
			contextMenu.parentNode.removeChild(contextMenu);
			contextMenu = null;
		}
	}
});

// Display the appropriate section when clicking on one of the tabs
var taskTab = document.getElementById('task_tab');
var reportTab = document.getElementById('report_tab');
var taskSection = document.getElementsByClassName('task_section')[0];
var reportSection = document.getElementsByClassName('report')[0];
reportSection.style.display  = 'none';

listeners.add(taskTab, 'click', function(e){
	var tab = e.target;
	var classList = tab.className.split(' ');
	if(!classList.includes('selected')){
		classList.push('selected');
		tab.className = classList.join(' ');
		reportTab.className = 'tab';

		taskSection.style.display  = '';
		reportSection.style.display  = 'none';
	}

});
listeners.add(reportTab, 'click', function(e){
	var tab = e.target;
	var classList = tab.className.split(' ');
	if(!classList.includes('selected')){
		classList.push('selected');
		tab.className = classList.join(' ');
		taskTab.className = 'tab';

		taskSection.style.display  = 'none';
		reportSection.style.display  = '';
	}
});

/* 
 * Callback functions
 */

function new_task(){
	/* Add a new task to the list */
	taskList.add();
}

function new_project(){
	/* Add a new project to the list  */

	var projectName = prompt("Entrer le nom du projet");

	if(projectList.includes(projectName)){
		alert('Le projet ' + projectName + ' existe déjà');
	}
	else{
		if(projectName !== null && projectName !== ''){
			projectList.add(projectName);
			taskList.update_project_lists(projectName);
		}
	}
}

function tasks_of_the_day(){
	/* Reschedule past,not done/cancelled tasks to current day then filter on
	 * tasks for current day */

	var currentDay = new Date();

	for(var i = 0; i < taskList.length; i++){
		var task = taskList[i];
		var row = task.row;
		var dateCell = row.get_child(columns.schedule);
		var picker = dateCell.get_child(0);
		var date = picker.container.valueAsDate;

		if (currentDay.newer(date) && ![statuses.done,statuses.cancelled].includes(task.status) ){
			picker.container.valueAsDate = currentDay;
			picker.container.dispatchEvent(new Event('change'));
			dateCell.value = picker.container.valueAsDate.get_full_date();
		}
	}

	filter.add(_new(Criterion,'schedule',[currentDay.get_full_date()]));
	filter.apply();
}

function postpone(){
	/* Reschedule current day's task that are not done/cancelled to next week
	 * day */

	if(!confirm("Reporter les tâches incomplètes au jour ouvré suivant ?")) return;
	var currentDay = new Date();
	var saturday = 6;
	var sunday = 0;

	for(var i = 0; i < taskList.length; i++){
		var task = taskList[i];
		var row = task.row;
		var dateCell = row.get_child(columns.schedule);
		var picker = dateCell.get_child(0);
		var date = picker.container.valueAsDate;

		if([statuses.done,statuses.cancelled].includes(task.status)
			|| date.get_full_date() !== currentDay.get_full_date()){
			continue;
		}
		
		date.setDate(date.getDate()+1);
		while([saturday,sunday].includes(date.getDay())){
			date.setDate(date.getDate()+1);
		}

		picker.container.valueAsDate = date;
		picker.container.dispatchEvent(new Event('change'));
		taskList[i].schedule = date;
	}
}
