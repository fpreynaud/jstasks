function _super(mother, child){
	mother.init.apply(child, arguments[2]);
}

var _Element = {
	type:'_Element',
	is_custom_element:true,
	init:function(container='div',attributes = {}){
		var children = to_list(arguments).slice(2);
		if(typeof container === 'string'){
			this._container_ = document.createElement(container);
		}
		else{
			this._container_ = container;
		}
		this.children = [];
		var atts = attributes.keys();
		for(var i = 0;i < atts.length;i++){
			var attribute = atts[i];
			this._container_.setAttribute(attribute, attributes[attribute]);
		}
		for (const child of children){
			if(typeof child === 'string' || typeof child === 'number'){
				var textChild = document.createTextNode(child.toString());
				this._container_.appendChild(textChild);
				this.value = child;
				this.children.push(textChild);
			}
			else if(child.is_custom_element){
				this.append(child);
			}
			else{
				this._container_.appendChild(child);
				this.children.push(child);
			}
		}
		this._container_.jsobj = this;
	},
	set_style:function(property,value){
		this.container.style[property] = value;
	},
	add_class:function(className){
		var currentClassName = this.container.className;
		var currentClassList = currentClassName.split(' ');
		if(!currentClassList.includes(className)){
			currentClassList.push(className);
			this.container.className = currentClassList.join(' ');
		}
	},
	remove_class:function(className){
		var currentClassName = this.container.className;
		var currentClassList = currentClassName.split(' ');
		var newClassList = [];
		for(const currentClass of currentClassList){
			if(currentClass !== className){
				newClassList.push(currentClass);
			}
		}
		this.container.className = newClassList.join(' ');
	},
	get_child:function(index){
		var child = null;
		if(index < this.children.length){
			child = this.children[index];
		}
		return child
	},
	addEventListener:function(event,listener){
		this._container_.addEventListener(event, listener);
	},
	append:function(){
		for(var i = 0; i < arguments.length; i++){
			var element = arguments[i];
			this._container_.appendChild(element._container_);
			this.children.push(element);
		}
	},
	_get_firstElementChild_:function(){ return this._container_.firstElementChild; },
	_get_value_:function(){return this._container_.value;},
	_set_value_:function(v){this._container_.value = v;},
	get firstElementChild() {return this._get_firstElementChild_();},
	get value(){return this._get_value_();},
	get container(){return this._container_;},
	set container(cont){
		if(cont !== undefined){
			this._container_ = cont;
			this._container_.jsobj = this;
		}
	},
	set value(v){this._set_value_(v);},
	set class(v){this.container.className = v;},
	get class(){return this.container.className;}
};

var TableRow = _extend(_Element,{
	type:'TableRow',
	init:function(container = null){
		if(!container){
			container = 'tr';
		}
		_super(_Element,this,[container,{'class':'task_row'}]);
	}
});

var Cell = _extend(_Element,{
	type:'Cell',
	init:function(container = null){
		if(!container){
			container = 'td';
		}

		_super(_Element,this,[container].concat(slice(arguments,1)));
	}
});

var ScrollableCell = _extend(Cell, {
	type:'ScrollableCell',
	init:function(value='', placeholder='', container='td'){
		_super(Cell,this,[container].concat(slice(arguments,3)));

		Object.defineProperty(this,'container',{set:this._set_container_});
		Object.defineProperty(this,'value',{set:this.set_value, get:this.get_value});


		if(typeof container === 'string'){
			var textarea = _new(_Element,'textarea',{'placeholder':placeholder,'class':'edit_area'}) 
			this.append(textarea);
		}
		else{
			var textarea = _new(_Element, container.firstElementChild);
			this.children.push(textarea);
		}

		this.value = value;
	},
	disable:function(){
		this.firstElementChild.setAttribute('disabled',true);
		return this;
	},
	get_value:function(){
		return this.get_child(0).container.value;
	},
	set_value:function(value){
		var textArea = this.get_child(0);
		textArea.value = value;
		textArea.container.value = value;
	},
	_set_container_:function(cont){
		this._container_ = cont;
		this._container_.jsobj = this;
		this.children = [];
		var textarea = _new(_Element,'textarea',{'placeholder':placeholder,'class':'edit_area'}) 
		textarea.value = cont.firstElementChild.value;
		this.children.push(textarea);
	}
});

var DropdownCell = _extend(Cell,{
	type:'DropdownCell',
	init:function(values=[],container='td'){
		_super(Cell,this,[container].concat(slice(arguments,2)));

		if(typeof container === 'string'){
			var dropdown = _new(_Element,'select',{'class':'dropdown'});
			for(var i=0; i<values.length; i++){
				var value = document.createTextNode(values[i]);
				var option = _new(_Element,'option',{'class':'dropdown_option'},value);
				option.value = values[i];
				dropdown.append(option);
			}
			this.append(dropdown);
		}
		else{
			var dropdown = _new(_Element, container.firstElementChild);
			for(var i = 0; i < dropdown.container.children.length; i++){
				var optionContainer = dropdown.container.children[i];
				var option = _new(_Element,optionContainer);
				option.value = optionContainer.value;
				dropdown.children.push(option);
			}
			this.children.push(dropdown);
		}
		this.value = dropdown.get_child(dropdown.container.selectedIndex).value;
		listeners.add(this.container,'change',this);
		Object.defineProperty(this,'container',{set:this._set_container});
	},
	get_selected:function(){
		var dropdown = this.get_child(0);
		return dropdown.get_child(dropdown.container.selectedIndex);
	},
	select:function(value){
		var dropdown = this.get_child(0);
		var option = dropdown.get_child(0);

		this.value = value;
		for(var i = 1; option !== null; i++){
			if(option.value === value){
				option.container.selected = true;
			}
			else{
				option.container.selected = false;
			}
			option = dropdown.get_child(i);
		}
	},
	color_options:function(scheme){
		var options = this.get_child(0).children;
		for(var i = 0; i < options.length; i++){
			var optionClass = scheme.get(options[i].value);
			if(optionClass === undefined){
				continue;
			}
			options[i].add_class(optionClass);
		}
	},
	handleEvent:function(e){
		switch(e.type){
			case 'change':
				this.value = e.target.value;
				break;
		}
	},
	_set_container:function(container){
		this._container_ = container;
		this.children = [];
		var dropdown = _new(_Element,'select',{'class':'dropdown'});
		this.children.push(dropdown);
		this._container_.jsobj = this;
		var select = this._container_.firstElementChild;
		for(var child = select.firstElementChild;child !== null && child !== undefined; child = child.nextElementSibling){
			var option = _new(_Element,'option',{'class':child.className},child.value);
			dropdown.append(option)
		}
		listeners.add(this._container_,'change',this);

	},
	set container(container){
		this._set_container(container);
	}
});

var DatePickerCell = _extend(Cell,{
	type:'DatePickerCell',
	init:function(container='td'){
		_super(Cell, this, [container].concat(slice(arguments,1)));

		if(typeof container === 'string'){
			this.append(_new(_Element,'input',{'type':'date','class':'date_picker'}));
			this.firstElementChild.valueAsDate = new Date();
			this.value = (new Date()).get_full_date();
		}
		else{
			var datePicker = _new(_Element,container.firstElementChild);
			this.value = datePicker.container.valueAsDate.get_full_date();
			this.children.push(datePicker);
		}
		Object.defineProperty(this,'container',{set:this._set_container_,get:this._get_container_});
	},
	_set_container_:function(cont){
		this._container_ = cont;
		this._container_.jsobj = this;
		this.value = cont.firstElementChild.valueAsDate;
	},
	_get_container_:function(){return this._container_;},
});

var ReportCell = _extend(_Element,{
	type:'ReportCell',
	init:function(duration=0,topics=_new(KVList)){
		_super(_Element,this,['div',{'class':'report_cell'}]);
		this.totalDuration = duration;
		this.topics = topics;
		this.summary = _new(_Element,'p',{'class':'report_contents'},'Durée totale: ' + this.format_duration(duration));
		this.append(this.summary);
		this.topicList = _new(_Element,'ul',{'class':'topic_list'});
		this.append(this.topicList)

		for(var i = 0; i < topics.length; i++){
			var kv = topics[i];
			var topic = kv[0];
			var topicDuration = kv[1];
			var topicListItem = _new(_Element,'li',{'class':'topic_list_item'},topic + ':' + this.format_duration(topicDuration));
			this.topicList.append(topicListItem);
		}
	},
	update_content:function(){
		this.summary.value = 'Durée totale: ' + this.format_duration(this.totalDuration);
		this.summary.container.innerHTML = this.summary.value;
		var topicListItem = this.topicList.get_child(0);

		for(var i = 0; i < this.topics.length; i++){
			var kv = this.topics[i];
			var topic = kv[0];
			var duration = kv[1];
			var inContent = false;

			for(var item = 0;topicListItem !== null; item++){
				var s = topicListItem.container.innerHTML;
				var l = s.split(':');
				var itemTopic = l[0];
				var itemDuration = this.parse_duration_string(l[1]);
				if(itemTopic === topic){
					itemDuration = duration;
					topicListItem.container.innerHTML = topic + ':' + this.format_duration(itemDuration);
					inContent = true;
					break;
				}
				topicListItem = this.topicList.get_child(item);
			}
			if(inContent){
				continue;
			}
			this.topicList.append(_new(_Element,'li',{'class':'topic_list_item'},topic + ':' + this.format_duration(duration)));

		}
	},
	parse_duration_string:function(string){
		var re = /(\d+)\s*h(\d+)\s*m(\d+)\s*s/
		var groups = string.match(re);
		var hours = parseInt(groups[1]);
		var minutes = parseInt(groups[2]);
		var seconds = parseInt(groups[3]);
		return hours*3600 + minutes*60 + seconds;
	},
	update:function(duration,topics){
		this.totalDuration += duration;
		for(var i = 0; i < topics.length; i++){
			var kv = topics[i];
			var topic = kv[0];
			var topicDuration = kv[1];
			if(this.topics.contains(topic)){
				this.topics.set(topic,this.topics.get(topic) + topicDuration);
			}
			else{
				this.topics.set(topic,topicDuration);
			}
		}
		this.update_content();
	},
	format_duration:function(duration){
		var hours = Math.floor(duration/3600);
		var minutes = Math.floor((duration - hours*3600)/60);
		var seconds = Math.floor((duration - hours*3600 - minutes*60));
		var day = 6*3600 + 30*60;
		var dayFraction = 0.25 * Math.round(4 * duration/day);

		return hours + 'h' + minutes + 'm' + seconds + 's,(' + dayFraction + ' jour)'
	}
});

var FilterMenuItem = _extend(_Element,{
	type:'FilterMenuItem',
	init:function(value){
		_super(_Element, this, ['div',{'class':'filter_menu_item'}]);
		Object.defineProperty(this, 'selected', {set:this._set_selected, get:this._get_selected});
		this._selected = true
		this.value = value;
		this.checkbox = _new(_Element,'input',{'type':'checkbox'});
		this.checkbox.container.checked = true;
		this.label = _new(_Element,'label',{},value);
		this.append(
			this.checkbox,
			this.label
		);

		var index = listeners.add(this.checkbox,'change',function(e){
			var item = e.target.parentNode.jsobj;
			item.selected = !item.selected;
		});
	},
	_set_selected:function(bool){
		this.checkbox.container.checked = bool;
		this._selected = bool;
	},
	_get_selected:function(){return this._selected;}
	
});

var FilterButton = _extend(_Element, {
	type:'FilterButton',
	init:function(){
		_super(_Element, this, ['button',{'class':'filter_button'},'Filtrer']);
	}
});

var FilterMenu = _extend(_Element,{
	type:'FilterMenu',
	init:function(column,values,operator = '='){
		_super(_Element, this, ['div',{'class':'filter_menu'}]);

		this.columnName = column.value;
		var th = column.parentNode;
		var tr = th.parentNode;
		this.columnIndex = [].indexOf.call(tr.children, th); 
		this.items = [];
		this._valuesContainer_ = _new(_Element,'div',{'class':'filter_menu_values'});

		var topFilterButton = _new(FilterButton);
		var bottomFilterButton = _new(FilterButton);
		var selectAllCheckbox = _new(FilterMenuItem, 'Sélectionner tout');

		var vc = this._valuesContainer_;

		// Select or deselect all values when clickin on the "Select all" checkbox
		listeners.add(selectAllCheckbox.checkbox, 'change', function(e){
			var itemsList = vc._container_.querySelectorAll('.filter_menu_item');
			for (const itemDiv of itemsList){
				itemDiv.jsobj.checkbox.container.checked = itemDiv.jsobj.selected = this.checked;
			}
		});

		// Add the top "Filter" button, the "Select all" checkbox, and the container for the values
		this.append(topFilterButton);
		this.append(selectAllCheckbox);
		this.append(this._valuesContainer_);

		// Add the values
		for(const value of values){
			var selected = false;
			if (filter.contains(this.columnIndex, value)){
				selected = true;
			}
			this.add_value(value.toString(), selected);
		}

		// Add the bottom "Filter" button
		this.append(bottomFilterButton);

		var col = this.columnIndex;
		var vals = this.items;

		// Apply filter when clicking on one of the "Filter" buttons
		(function(){
			var column = col;
			var values = vals;
			var index = listeners.add(
				topFilterButton,'click',
				function(e){
					var criteria = [];
					for(const item of values){
						if(item.selected){
							criteria.push(item.value);
						}
					}
					filter.add(_new(Criterion,column,criteria,operator));
					filter.apply();
					e.target.parentNode.parentNode.removeChild(e.target.parentNode);
					contextMenu = null;
				}
			);
			listeners.add(bottomFilterButton,'click',listeners.get_function(index-1));
		})();
	},
	add_value:function(value, selected=true){
		var item = _new(FilterMenuItem,value);
		item.selected = selected;
		this.items.push(item);
		this._valuesContainer_.append(item);
	},
	add_folder:function(folder){
		//this.items.push(folder.header);
		this._valuesContainer_.append(folder);
	}
});

var FoldButton = _extend(_Element,{
	type:'FoldButton',
	init:function(folded=true){
		this.folded = folded;
		_super(_Element, this,['button', {'class':'foldbutton','style':'width:20px;text-align:center;padding:0px'}, this.folded ? '+':'-']);
		listeners.add(this,'click',this);
	},
	handleEvent:function(e){
		this.folded = !this.folded;
		this.container.innerHTML = this.folded ? '+':'-';
	}
});
var Folder = _extend(_Element, {
	type:'Folder',
	init:function(text,folded=true,values=[],margin=0){
		_super(_Element, this);
		this.margin = margin;
		this.set_style('margin-left',margin*12 + 'px');
		this.class = 'folder';
		this.folded = folded;
		var button = _new(FoldButton, this.folded);
		this.header = _new(FilterMenuItem, text);
		this.header.class = 'folder_header';
		this.content = _new(_Element,'div',{'class':'folder_content'});
		this.add_values(values);
		this.append(button,this.header,this.content);

		var o = this;
		listeners.add(button,'click',function(e){o.change_fold_state()});
		listeners.add(this.header.checkbox, 'change',function(e){
			o.propagate_select();
		});
		this.header.selected = false;

	},
	add_subfolder:function(text,folded=true){
		var subfolder = _new(Folder,text,folded,[],margin=this.margin+1);
		this.content.append(subfolder);
		return subfolder;
	},
	add_values:function(values){
		var ret = [];
		for(const text of values){
			var item = _new(FilterMenuItem, text);
			item.set_style('margin-left',(this.margin+2)*12 + 'px');
			this.content.append(item);
			ret.push(item);
		}
		return ret;
	},
	change_fold_state:function(){
		this.folded = !this.folded;
		this.content.set_style('display',this.folded ? 'none':'');
	},
	propagate_select:function(){
		this.header.selected = false;
		var child = this.content.get_child(0);
		for(var i = 0;child !== null && child !== undefined; child = this.content.get_child(i++)){
			if(typeof child.header !== 'undefined'){
				child.header.selected = false;
				child.header.checkbox.container.checked = this.header.checkbox.container.checked;
				child.propagate_select();
				continue;
			}
			else{
				child.checkbox.container.checked = this.header.checkbox.container.checked;
				child.selected = this.header.checkbox.container.checked;
			}
		}
	}
});

var contextMenu = null;
function display_menu(target,menu,e){
	if(contextMenu !== null){
		contextMenu.parentNode.removeChild(contextMenu);
	}
	contextMenu = menu;
	menu.style.top = e.layerY;
	menu.style.left = e.layerX;
	target.parentNode.appendChild(menu);
}

function get_element_child(element, index){
	var ret = element.firstElementChild;
	for(var i = 1;i<=index && ret !== null;i++){
		ret = ret.nextElementSibling;
	}
	return ret;
}
