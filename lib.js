function _extend(proto, obj){
	var o = Object.create(proto);
	for(var att in obj){
		o[att] = obj[att];
	}
	return o;
}

function _new(proto){
	var o = Object.create(proto);
	var args = [];

	for (var i = 1; i < arguments.length; i++){
		args.push(arguments[i]);
	}

	if(proto.hasOwnProperty('init')){
		proto.init.apply(o, args);
	}
	return o;
}

function log(){
	console.log.apply(console,arguments)
}

function to_list(iterable){
	ret = [];
	for (element of iterable){
		ret.push(element);
	}
	return ret;
}

var KVList = _extend([],{
	type:'KVList',
	init:function(key=null,value=null){
		if (key !== null && value !== null){
			this.push([key,value])
		}
	},
	from_list:function(l){
		for(const kv of l){
			this.push(kv);
		}
		return this;
	},
	get:function(key, index=null){
		var ret = undefined;
		if(index === null){
			for(var element of this){
				if (key === element[0]){
					ret = element[1];
				}
			}
		}
		else{
			ret = this[index][1];
		}
		return ret;
	},
	indexOf:function(key){
		var ret = null;
		for(var i=0;i<this.length;i++){
			if(this[i][0] === key){
				ret = i;
				break;
			}
		}
		return ret;
	},
	set:function(key,value){
		try{
			var index = this.indexOf(key);
			this[index][1] = value;
		}
		catch{
			this.push([key,value]);
		}
	},
	contains:function(key){
		var ret = false;

		for(var element of this){
			if ( key === element[0]){
				ret = true;
				break;
			}
		}

		return ret;
	},
	toString:function(){
		var s = '['
		for(const kv of this){
			s += '\n  (' + kv[0] + ', ' + kv[1] + ')';
		}
		s += '\n]'
		return s;
	}
});

var Week = _extend([],{
	init:function(){
		var	monday = new Date();
		monday.setDate(monday.getDate()-(monday.getDay()+6)%7);
		var day = new Date(monday);
		for(var i=0; i<7; i++){
			this.push(new Date(day));
			day.setDate(day.getDate() + 1)
		}
	},
	set_current:function(){
		var	monday = new Date();
		monday.setDate(monday.getDate()-(monday.getDay()+6)%7);
		var day = new Date(monday);
		for(var i=0; i<7; i++){
			this[i] = new Date(day);
			day.setDate(day.getDate() + 1)
		}
	},
	is_in_week:function(date){
		var ret = false;
		for(const day of this){
			if(day.get_full_date() === date){
				ret = true;
				break;
			}
		}
		return ret;
	}
});

Date.prototype.get_full_date = function(){
	var fullDate = this.getDate().toString().padStart(2,'0') + '/' + (this.getMonth() + 1).toString().padStart(2,'0') + '/' + this.getFullYear().toString().padStart(4,'0');
	return fullDate;
}

Date.prototype.older = function(date){
	return compare_dates(this.get_full_date(), date.get_full_date()) < 0
}
Date.prototype.newer = function(date){
	return compare_dates(this.get_full_date(), date.get_full_date()) >= 0
}

function compare_dates(date1,date2){
	var d1 = date1.split('/');
	var d2 = date2.split('/');
	var year1 = parseInt(d1[2]), month1 = parseInt(d1[1]), day1 = parseInt(d1[0]);
	var year2 = parseInt(d2[2]), month2 = parseInt(d2[1]), day2 = parseInt(d2[0]);
	var ret = 0;

	if(year1 === year2){
		if(month1 === month2){
			ret = day1 - day2;
		}
		else{
			ret = month1-month2;
		}
	}
	else{
		ret = year1-year2;
	}

	return ret>0?1:(ret === 0?0:-1); 

}

Object.prototype.keys = function(){
	var ret = []
	for(const k in this){
		if(this.hasOwnProperty(k)){
			ret.push(k);
		}
	}
	return ret
}
Object.prototype.values = function(){
	var ret = []
	for(const k in this){
		if(this.hasOwnProperty(k)){
			ret.push(this[k]);
		}
	}
	return ret
}

var Listeners = {
	init:function(){
		this.listeners = [];
	},
	add:function(target,event,listener){
		this.listeners.push([target,event,listener]);
		target.addEventListener(event, listener);
		return this.listeners.length;
	},
	remove:function(index){
		var listener = this.listeners[index];
		var target = listener[0];
		var event = listener[1];
		var listenFunc = listener[2];
		target.removeEventListener(event,listenFunc);
		this.listeners = this.listeners.slice(0,index).concat(this.listeners.slice(index+1));
	},
	get:function(index){
		return this.listeners[index];
	},
	get_target:function(index){
		return this.get(index)[0];
	},
	get_event:function(index){
		return this.get(index)[1];
	},
	get_function:function(index){
		return this.get(index)[2];
	}
};

function slice(iterable, low, high){
	return [].slice.call(iterable, low, high);
}
